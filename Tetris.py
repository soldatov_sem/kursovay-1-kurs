import pygame as pg
import sys
from copy import deepcopy
from random import choice, randrange

WIDTH, HEIGHT = 10, 18
TILE = 35
GAME_SIZE = WIDTH * TILE, HEIGHT * TILE
RES = 540, 650
FPS = 60

pg.init()
# Всё игровое пространство
sc = pg.display.set_mode(RES)
# Только игровое поле (где перемещаются тетрамино)
game_sc = pg.Surface(GAME_SIZE)
clock = pg.time.Clock()
# сетка
grid = [pg.Rect(x * TILE, y * TILE, TILE, TILE) for y in range(HEIGHT) for x in range(WIDTH)]

fig_positions = [[(-1, 0), (-2, 0), (0, 0), (1, 0)],
               [(0, -1), (-1, -1), (-1, 0), (0, 0)],
               [(-1, 0), (-1, 1), (0, 0), (0, -1)],
               [(0, 0), (-1, 0), (0, 1), (-1, -1)],
               [(0, 0), (0, -1), (0, 1), (-1, 0)],
               [(0, 0), (0, -1), (0, 1), (1, -1)],
               [(0, 0), (0, -1), (0, 1), (-1, -1)]]

# Создаем список экземпляром Rect, который описывает тетрамино "квадрат"
rectangle_rect = [pg.Rect(x + WIDTH // 2, y + 1, 1, 1) for x, y in fig_positions[1]]
rectangle = deepcopy(rectangle_rect)
# Массив элементов класса Rect, каждый элемент которого - набор прямоугольных областей для одного тетрамино
figures = [[pg.Rect(x + WIDTH // 2, y + 1, 1, 1) for x, y in fig_pos] for fig_pos in fig_positions]
# Экзэмпляр для отрисовки составляющих частей фигур
figure_rect = pg.Rect(0, 0, TILE - 2, TILE - 2)
# Это карта игрового поля, на которой мы помечаем положение игровых фигур (каждый элемент - это квадрат опр. цвета на игровой сетке)
area = [[0 for i in range(WIDTH)] for j in range(HEIGHT)]

bg = pg.image.load('yellow.jpg').convert()

# Создание экземплятоп класса штрихов
main_font = pg.font.Font('font.ttf', 50)
font = pg.font.Font('font.ttf', 40)

# Метод render создаёт поверхность, на которой будет размещён текст нужным шрифтом
# Второй арг - сглаживание, третий - цвет
title_tetris = main_font.render('TETRIS', True, pg.Color('Red   '))
title_score = font.render('score:', True, pg.Color('white'))
title_record = font.render('record:', True, pg.Color('white'))
num_lines = font.render('lines', True, pg.Color('White'))


def receive_color():
    col = (randrange(30, 256), randrange(30, 256), randrange(30, 256))
    while col == (255, 223, 88):
        col = (randrange(30, 256), randrange(30, 256), randrange(30, 256))
    return col


figure, next_figure = deepcopy(choice(figures)), deepcopy(choice(figures))
color, next_color = receive_color(), receive_color()

scores = {0: 0, 1: 100, 2: 300, 3: 700, 4: 1100}
score, lines = 0, 0

# Загружаю музыку и устанавливаю громкость
sound1 = pg.mixer.Sound('Score.mp3')
sound2 = pg.mixer.Sound('game_over.mp3')
sound3 = pg.mixer.Sound('theme_tetris.mp3')
channel1 = pg.mixer.Channel(1)
sound3.set_volume(0.3)

# зацикливаю музыку на фоне с помощью "-1"
sound3.play(-1)

# проверка границ перемещения по горизонтали, вертикали и наложении на уже упавшую фигуру
def borders_checking(auxiliary_fig):
    if auxiliary_fig.x < 0 or auxiliary_fig.x > WIDTH - 1:
        return False
    if auxiliary_fig.y > HEIGHT - 1 or area[auxiliary_fig.y][auxiliary_fig.x]:
        return False
    return True

def get_record():
    try:
        with open('record.txt') as f:
            return f.read()
    except FileNotFoundError:
        with open('record.txt', 'w') as f:
            f.write('0')
            return 0


def set_record(new_record, score):
    rec = max(int(new_record), score)
    with open('record.txt', 'w') as f:
        f.write(str(rec))


# проверка на то, чтобы новая фигура не была такой же, как старая
def choice_next_figure():
    next_figure = choice(figures)
    flag = False
    while not flag:
        if next_figure != figure:
            return next_figure
        next_figure = choice(figures)


printed_lines = 0
anim_count, anim_speed, anim_border = 0, 40, 2200
while 1:
    record = get_record()
    shift_x, rotate = 0, False
    # Расставлю 2 картинки на 2 поверхности внутри игры
    # blit - метод, который размещает текст с центром в (0, 0) на поверхности bg

    sc.blit(bg, (0, 0))
    sc.blit(game_sc, (10, 10))
    game_sc.fill((255, 223, 88))

    # delay for full lines
    for i in range(lines):
        pg.time.wait(200)
    # идём по списку произошедших событий
    for event in pg.event.get():
        if event.type == pg.QUIT:
            pg.quit()
            sys.exit()
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_LEFT:
                shift_x = -1
            elif event.key == pg.K_RIGHT:
                shift_x = 1
            elif event.key == pg.K_DOWN:
                anim_border = 150
            elif event.key == pg.K_UP:
                rotate = True
        elif event.type == pg.KEYUP and event.key == pg.K_DOWN:
            anim_border = 2200

    # движение по х
    fig_old = deepcopy(figure)
    rect_old = deepcopy(rectangle)
    for i in range(4):
        figure[i].x += shift_x
        rectangle[i].x += shift_x
        if not borders_checking(figure[i]):
            # Если наша фигура выходит за границы, то мы заменяем её на копию, которая находится в нужном диапозоне
            figure = deepcopy(fig_old)
            rectangle = deepcopy(rect_old)
            break

    # Этим условием мы создаём задержку, чтобы тетрамино не падало почти мгновенно
    anim_count += anim_speed
    if anim_count > anim_border:
        anim_count = 0
        fig_old = deepcopy(figure)
        now, flag = 0, True
        # движение по y
        while now < 4 and flag:
            figure[now].y += 1
            rectangle[now].y += 1
            if not borders_checking(figure[now]):
                for j in range(4):
                    # заполняем карту нужным цветом
                    area[fig_old[j].y][fig_old[j].x] = color
                rectangle = deepcopy(rectangle_rect)    
                figure, color = next_figure, next_color
                next_figure, next_color = deepcopy(choice_next_figure()), receive_color()
                anim_border = 2200
                flag = False
            now += 1

    # поворот фигуры
    center = figure[0]
    fig_old = deepcopy(figure)
    if rotate and figure != rectangle:
        now, flag = 0, True
        while now < 4 and flag:
            y = figure[now].x - center.x
            x = figure[now].y - center.y
            figure[now].y = center.y + y
            figure[now].x = center.x - x
            if not borders_checking(figure[now]):
                figure = deepcopy(fig_old)
                flag = False
            now += 1

    # Проверка на заполнение линий
    line, lines = HEIGHT - 1, 0
    for row in range(HEIGHT - 1, -1, -1):
        kol = 0
        for column in range(WIDTH):
            if area[row][column]:
                kol += 1
            area[line][column] = area[row][column]
        if kol < WIDTH:
            line -= 1
        else:
            anim_speed += 1
            lines += 1
    # если была удалена хотя бы 1 линия, то проигрывается аудио дорожка
    if lines:
        sound1.set_volume(0.5)
        sound1.play()

    # увеличиваю счётчик удаленных линий
    printed_lines += lines
    score += scores[lines]
    # рисую сетку черного цвета, у которой видны только границы из-за аргумента "1" - толщины границы
    [pg.draw.rect(game_sc, (250, 250, 250), i_rect, 1) for i_rect in grid]

    # рисую фигуры
    for i in range(4):
        figure_rect.x = figure[i].x * TILE
        figure_rect.y = figure[i].y * TILE
        pg.draw.rect(game_sc, color, figure_rect)

    # рисую карту уже упавших фигур
    for y, row in enumerate(area):
        for x, col in enumerate(row):
            if col:
                figure_rect.x, figure_rect.y = x * TILE, y * TILE
                pg.draw.rect(game_sc, col, figure_rect)

    # рисую следующую фигуру за игровым полем
    for i in range(4):
        figure_rect.x = next_figure[i].x * TILE + 270
        figure_rect.y = next_figure[i].y * TILE + 185
        pg.draw.rect(sc, next_color, figure_rect)

    # рисую подписи в игровом пространстве
    sc.blit(title_tetris, (385, 30))
    sc.blit(title_score, (405, 430))
    new_record = max(int(record), score)
    sc.blit(font.render(str(score), True, pg.Color('white')), (405, 460))
    sc.blit(num_lines, (405, 350))
    sc.blit(font.render(str(printed_lines), True, pg.Color('white')), (405, 380))
    sc.blit(title_record, (405, 520))
    sc.blit(font.render(str(new_record), True, pg.Color('white')), (405, 550))

    # проверка на завершения игры
    for column in range(WIDTH):
        if area[0][column]:
            sound3.stop()
            printed_lines = 0
            channel1.play(sound2)
            pg.time.delay(200)
            set_record(new_record, score)
            area = [[0 for row in range(WIDTH)] for row in range(HEIGHT)]
            anim_count, anim_speed, anim_border = 0, 40, 2200
            score = 0
            # при заверщении игры заполняю игровое поле цветом заднего фона
            for i_rect in grid:
                pg.draw.rect(game_sc, (255, 223, 88), i_rect)
                sc.blit(game_sc, (10, 10))
                pg.display.flip()
                pg.time.delay(8)
            sound1.set_volume(0.3)
            sound3.play(-1)
    # обновляю игровое пространство
    pg.display.update()
    # Метод экземпляра класса Сlock, который сам расчитывает задержку
    clock.tick(FPS)


